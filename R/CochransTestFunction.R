#####                        Cochran's Test                       ######
##                        Author: Bobby Resch                         ##

cochran.test<-function(x, a=0.05){
  
  c<-ncol(x)
  c.tots<-apply(x,2,sum)
  r.tots<-apply(x,1,sum)
  
  T<-((c*(c-1)*sum(c.tots^2))-((c-1)*(sum(x)^2))) / (c*sum(x) - (sum(r.tots^2)))
  
  cv<-qchisq(a,df=(c-1), lower.tail = FALSE)
  pv<-pchisq(T,df=(c-1), lower.tail = FALSE)
  
  if(T > cv){print(paste0(cat("The Test Statistic T = ", T, " is greater than the CV =", cv,
                     "\n Reject the Null Hypothesis with p-Value =", pv,"\n"), ''))} else{
                       print(paste0(cat("The Test Statistic T = ", T, " is not greater than the CV =", cv,
                                 "\n Fail to Reject the Null Hypothesis with p-Value =", pv, "\n"),'')) 
                     }
}