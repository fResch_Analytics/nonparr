#####                        Binomial Test                        ######
##                        Author: Bobby Resch                         ##


bin.test<-function(x,n,p0=0.5, type='twotailed', a=0.05, conf.level=0.95)
{
  pr<-c()
  
  if(n>=30){
  lower<-max(((x/n) - qnorm((1-(a/2)), mean=0, sd=1)*sqrt(((x*(n-x))/(n^3)))),0)
  upper<-min(((x/n) + qnorm((1-(a/2)), mean=0, sd=1)*sqrt(((x*(n-x))/(n^3)))),1)
  } else
  {
    alpha=(1-conf.level)/2
    p.L <- function(x, alpha) {
      if (x == 0) 
        0
      else qbeta(alpha, x, n - x + 1)
    }
    p.U <- function(x, alpha) {
      if (x == n) 
        1
      else qbeta(1 - alpha, x + 1, n - x)
    }
    
    lower<-p.L(x,alpha)
    upper<-p.U(x,alpha)
  }
  
  
  if(type=='twotailed')
  {
    Z<-min((1-pbinom(x,size=n,prob=p0)),pbinom(x,size=n,prob=p0))
    
    cv1<-qbinom((a), size=n, prob=p0)
    cv2<-qbinom((1-(a)), size=n, prob=p0)
    

    if (Z < (a/2) || Z> 1-(a/2) )
    { print(paste(cat("Reject the Null Hypothesis with p-Value : p= ", min(2*Z, 2*(1-Z)),"\n"), 
                     cat("The critical values are : cv1 = ", cv1, " and cv2 = ", cv2, "\n"),
                      "The Confidence Interval is : (", lower, "," , upper, ")"))} else 
    {
      print(paste(cat("Do Not Reject the Null Hypothesis", "\n"),
                     cat("The number of successes falls between the critical values:
                      cv1 = ", cv1, " and cv2 = ", cv2, "\n"),
                      "The Confidence Interval is : (", lower, "," , upper, ")"))  
    }
  } else
  {
    if(type=='lefttailed')
    {
      Z<-pbinom(x,size=n,prob=p0)
      
      cv1<-qbinom((a), size=n, prob=p0)
      
      if (Z < (a) )
      { print(paste(cat("Reject the Null Hypothesis with p-Value : p= ", Z,"\n"),
                       cat("The critical value is : cv1 = ", cv1, "\n"),
                      "The Confidence Interval is : (", lower, "," , upper, ")"))} else 
                       {
                         print(paste(cat("Do Not Reject the Null Hypothesis", "\n"),
                                    cat("The number of successes falls above the critical value: 
                                    cv1 = ", cv1, "\n"),
                                    "The Confidence Interval is : (", lower, "," , upper, ")"))
                       }
  } else
  {
    Z<-1-pbinom(x,size=n,prob=p0)
    
    cv1<-qbinom((1-a), size=n, prob=p0)
    
    if (Z < (a) )
    { print(paste(cat("Reject the Null Hypothesis with p-Value : p= ", Z,"\n"),
                 cat("The critical value is : cv1 = ", cv1, "\n"),
                 "The Confidence Interval is : (", lower, "," , upper, ")"))} else  
                 { 
                   print(paste(cat("Do Not Reject the Null Hypothesis", "\n"),
                              cat("The number of successes falls below the critical value: cv1 = ", cv1, "\n"),
                              "The Confidence Interval is : (", lower, "," , upper, ")"))
                 }
  }
  }
}