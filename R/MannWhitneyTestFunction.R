#####                          Mann-Whitney (Wilcoxon Signed Rank) Test                          ######
##                                        Author: Bobby Resch                                        ##

mw.test<-function(x,y,a=0.05, type="two-tailed")
{
  library(sqldf)

  m<-length(as.vector(x))
  n<-length(as.vector(y))
  N=m+n
  
  xmat<-as.matrix(cbind(x,c(rep("x",length(as.vector(x))))))
  ymat<-as.matrix(cbind(y,c(rep("y",length(as.vector(y))))))
  
  
  d<-as.data.frame(rbind(xmat,ymat), stringsAsFactors = F)
  colnames(d)<-c("Observation", "Population")
  
  attach(d)
  d<-d[order(as.numeric(Observation)),]
  detach(d)
  
  d$rank<-seq(1,length(d$Observation),1)
  
  d2<-sqldf('select Observation, avg(rank)[SR_rank] from d group by Observation')
  d3<-sqldf('select d.Observation
                , d.[Population], d2.SR_rank
            from
              d
            inner join d2
            on D.Observation = d2.Observation')
  
  T=sum(subset(d3, Population=='x')$SR_rank)
  T1<-(T - (n*((N+1)/2)) )/sqrt( ((n*m)/N*(N-1)) * (sum( (subset(d3, Population=='x')$SR_rank)^2) +
                                                         sum( (subset(d3, Population=='y')$SR_rank)^2)  ) - 
                                                              ( ((n*m)*((N+1)^2))/(4*(N-1))) )  
  
  if(m<=20 && n<=20){  
    if(type=="two-tailed"){
    alpha=a/2
    cv1<-m*(m+1)/2 + qwilcox(alpha, m, n, lower.tail = T)
    cv2<-m*(m+n+1)-cv1
    
    print(paste("The Lower CV is : ", cv1, " and the Upper CV is : ", cv2))
    
          if(T< cv1 || T>cv2){ 
            print(paste("The Test Statistic T = ", T, "is significant. Reject the null hypothesis"))} else{
              print(paste(cat("The Test Statistic T = ", T, "is not significant. \n"),"Do Not Reject the null hypothesis"))
            }
          }else{    if(type=="right-tailed"){
      alpha=a
      cv1<-m*(m+1)/2 + qwilcox(alpha, m, n, lower.tail = T)
      cv2<-m*(m+n+1)-cv1
      
      print(paste("The CV is : ", cv2))
            
            if(T>cv2){ 
              print(paste("The Test Statistic T = ", T, "is significant. Reject the null hypothesis"))} else{
                print(paste(cat("The Test Statistic T = ", T, "is not significant. \n"),"Do Not Reject the null hypothesis"))
              }      
            
      
    }else{      
      alpha=a
      cv1<-m*(m+1)/2 + qwilcox(alpha, m, n, lower.tail = T)
      cv2<-m*(m+n+1)-cv1
      
      print(paste("The CV is : ", cv1))
            
            if(T<cv1){ 
              print(paste("The Test Statistic T = ", T, "is significant. Reject the null hypothesis"))} else{
                print(paste(cat("The Test Statistic T = ", T, "is not significant. \n"),"Do Not Reject the null hypothesis"))
              }  
        }    
      }
    } else{      if(type=="two-tailed"){
        alpha=a/2
        cv1<-m*(N+1)/2 + pnorm(alpha, lower.tail = T)*sqrt(((m*n*(N+1))/12))
        cv2<-m*(N+1)/2 + pnorm(alpha, lower.tail = F)*sqrt(((m*n*(N+1))/12))
        
        print(paste("The Lower CV is : ", cv1, " and the Upper CV is : ", cv2))
              
              if(T< cv1 || T>cv2){ 
                print(paste("The Test Statistic T = ", T, "is significant. Reject the null hypothesis"))} else{
                  print(paste(cat("The Test Statistic T = ", T, "is not significant. \n"),"Do Not Reject the null hypothesis"))
                }      
              
              
      }else{         if(type=="right-tailed"){
          alpha=a
          cv1<-m*(N+1)/2 + pnorm(alpha, lower.tail = T)*sqrt(((m*n*(N+1))/12))
          cv2<-m*(N+1)/2 + pnorm(alpha, lower.tail = F)*sqrt(((m*n*(N+1))/12))
          
          print(paste("The CV is : ", cv2))
                
                if(T>cv2){ 
                  print(paste("The Test Statistic T = ", T, "is significant. Reject the null hypothesis"))} else{
                    print(paste(cat("The Test Statistic T = ", T, "is not significant. \n"),"Do Not Reject the null hypothesis"))
                  }      
                
                
        }else{
          alpha=a
          cv1<-m*(N+1)/2 + pnorm(alpha, lower.tail = T)*sqrt(((m*n*(N+1))/12))
          cv2<-m*(N+1)/2 + pnorm(alpha, lower.tail = F)*sqrt(((m*n*(N+1))/12))
          
          print(paste("The CV is : ", cv1))
                
                if(T<cv1){ 
                  print(paste("The Test Statistic T = ", T, "is significant. Reject the null hypothesis"))} else{
                    print(paste(cat("The Test Statistic T = ", T, "is not significant. \n"),"Do Not Reject the null hypothesis"))
                  }  
        }    
      }
        
    }
  
  
  
}