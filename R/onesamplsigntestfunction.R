#####                   One Sample Sign Test Function                   ######
##                            Author: Bobby Resch                           ##



onesamplesigntest<-function(x, md=median(x), type="twotailed")  ##Function takes a 1 dimensional                                                                       vector x, a declared median value,
                                                                ##and a type (either "twotailed", 
                                                                ##"righttailed", or "lefttailed")
{
  x<-as.vector(x)                                               ##Forces x into a vector format
  
  below<-subset(x, x < md )                                     ##Finds those below declared median
  tied<-subset(x, x == md )                                     ##Finds those equal to declared median
  above<-subset(x, x > md )                                     ##Finds thode above declared median
  
  T = length(above)                                             ##T is number above
  n=length(below) + length(above)                               ##n is the number of non-ties
  
  
  ## Below tabulates the p-vale associated with T, based on test type ##
  
  
  if (type == "twotailed" && T <= n/2 ) {p<- 2*(pbinom(T,n, 0.5)) } else{
    if(type == "twotailed" && T > n/2 ) {p<-2*(1-pbinom(T-1,n, 0.5)) }else{
      if (type == "righttailed") {p<- 1-pbinom(T-1,n, 0.5) }else {
        if (type == "lefttailed") {p<- pbinom(T,n, 0.5) } }}}
  
  list(paste("The test statistic is : T = ", T, " and the p-Value = ", p ))
}